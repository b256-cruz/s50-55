import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from "sweetalert2"

export default function Login() {

		const { user, setUser } = useContext(UserContext);

		const [email, setEmail] = useState('');
		const [password, setPassword] = useState('');

		const [isActive, setIsActive] = useState(false);

		console.log(email);
		console.log(password);

		useEffect (() => {
			if(email !== '' && password !== '') {

				setIsActive(true)

			} else {

				setIsActive(false)

			}
		})

		function loginUser(e) {

			e.preventDefault();

			fetch("http://localhost:4000/users/login",{
				method: "POST",
				headers: {
					"content-type": "application/json"
				},
				body: JSON.stringify({
					email: email,
					password: password
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				if(typeof data.access !== "undefined"){
					localStorage.setItem("token",data.access)
					retrieveUserDetails(data.access)

					Swal.fire({
						title: "Login Successful",
						icon: "success",
						text: "Welcome to Zuitt!"
					})

				} else {

					Swal.fire({
						title: "Authentication failed",
						icon: "error",
						text: "Check your login details and try again"
					})
				}
			})

			// localStorage.setItem("email", email)

			// setUser({
			// 	email: localStorage.getItem('email')
			// })

			// clear input fields
			setEmail('');
			setPassword('');

			// alert('Successful login');
		}

		const retrieveUserDetails = (token) => {
			fetch("http://localhost:4000/users/details",{
				header:{
					Authorization: "Bearer ${token}"
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				setUser({
					id:data._id,
					isAdmin: data.isAdmin
				})
			})
		}

		return (
			
			(user.id !== null) ?
				<Navigate to="/courses" />
			:
			<Form onSubmit={e => loginUser(e)}>
		      <Form.Group className="mb-3" controlId="formBasicEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="formBasicPassword">
		        <Form.Label>Password</Form.Label>
		        <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
		      </Form.Group>

		      {
		      	isActive ?
		      		<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
		      	:
		      		<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
		      }
		      
		    </Form>
		)
}
