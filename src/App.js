import './App.css';
import AppNavBar from './components/AppNavBar';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { UserProvider } from './userContext';
import Home from "./pages/Home"
import Courses from './pages/Courses';
import Register from "./pages/Register"
import Login from "./pages/Login"
import Logout from './pages/Logout';
import NotFound from './components/NotFound';
import CourseView from "./pages/CourseView"
import { useEffect, useState } from 'react';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect (() => {
    console.log(user)
    console.log(localStorage)
  })

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar />
        <Container>
          <Routes>
            <Route path = "/" element = {<Home />} />
            <Route path = "/courses" element = {<Courses />} />
            <Route path = "/register" element = {<Register />} />
            <Route path = "/login" element = {<Login />} />
            <Route path = "/logout" element= {<Logout />} />
            <Route path = "/courseView/:courseId" element = {<CourseView />} />
            <Route path = "*" element = {<NotFound />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
