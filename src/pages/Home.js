import Banner from "../components/Banner"
import Highlights from "../components/Highlights"

export default function HTMLModElement() {
    return (
        <>
            <Banner />
            <Highlights />
        </>
    )
}