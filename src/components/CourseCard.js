import { useState } from "react"
import { Button, Card} from "react-bootstrap"
import { Link } from "react-router-dom"

export default function CourseCard({courseProp}) {

    const {name, description, price, _id} = courseProp

    // const [seatsState, setCount] = useState(30)

    // function enroll() {
    //     if(seatsState <= 0) {
    //         return alert("No more seats")
    //     }
        
    //     setCount(seatsState - 1) 
    // }

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">Description:</Card.Subtitle>
                <Card.Text>
                        {description}
                </Card.Text>
                <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
                <Card.Text>
                        {price}
                </Card.Text>
                <Button variant="primary" as={Link} to= {`/courseView/${_id}`}>Details</Button>
            </Card.Body>
        </Card>
    )
}