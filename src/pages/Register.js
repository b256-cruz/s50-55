import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function Register() {

	const navigate = useNavigate()

	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [mobileNo, setMobileNo] = useState("")
	const [password1, setPassword1] = useState("")
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(firstName)
	console.log(lastName)
	console.log(mobileNo)
	console.log(password1);
	console.log(password2);

	useEffect (() => {
		if((email !== '' && password1 !== '' && password2 !== '' && firstName !== "" && lastName !== "" && mobileNo !=="" && mobileNo.length >= 11 ) && (password1 === password2)) {

			setIsActive(true)

		} else {

			setIsActive(false)

		}
	})

	function registerUser(e) {

		e.preventDefault();

		fetch("http://localhost:4000/users/checkEmail",{
			method: "POST",
			headers: {
				"Content-type": "application/json"
			},
			body: JSON.stringify({
				email: email,
			})
		})
		.then(res => res.json())
		.then((data) => {

			if(data === true) {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "email already exist"
				})
			}else {
				fetch("http://localhost:4000/users/register",{
			method: "POST",
			headers: {
				"Content-type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password1
			})
		})
		.then(res => res.json())
		.then((data) => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title: "Successfully Enrolled!",
					icon: "success",
					text: "You have successfully enrolled for this course"
				})

				navigate("/login")
			}else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
			}
		})
	}

	return (
		// (user.email !== null) ?
		// 		<Navigate to="/courses" />
		// :
		<Form onSubmit={e => registerUser(e)}>
			<Form.Group className="mb-3" controlId="formBasicFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)}/>
	        </Form.Group>

			<Form.Group className="mb-3" controlId="formBasicLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)}/>
	        </Form.Group>

	        <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	        </Form.Group>

			<Form.Group className="mb-3" controlId="formBasicMobileNumber">
                <Form.Label>Mobile Number</Form.Label>
				<br></br><Form.Text>Please Input 11 digit Number</Form.Text>
                <Form.Control type="tel" placeholder="Enter mobile Nummber" value={mobileNo} onChange={e => setMobileNo(e.target.value)}/>
	        </Form.Group>

	        <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)}/>
	        </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)}/>
            </Form.Group>

            {
                isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
                :
                    <Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
            }
	    </Form>
	)
}
